Role migrated here https://gitlab.devoping.com/devops/ansible-playbooks/-/tree/master/roles/install-php-fpm-v2

# Ansible-Apache-PHP-FPM

An Ansible role that install and configure PHP-FPM on Debian/Ubuntu and CentOS.

Based on geerlingguy/ansible-role-php
